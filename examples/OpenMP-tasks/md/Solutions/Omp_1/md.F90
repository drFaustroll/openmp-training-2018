Module md_data
!Parameters
!3 dimensional simulation
Integer, Parameter :: nparts = 50000 !Number of particles in the simulation
Integer, Parameter :: nsteps = 10 !number of time steps in the simulation
Integer, Parameter :: dp = Selected_Real_Kind(15,307) !Double precision

Real(kind=dp), Parameter :: mass = 1.0_dp !mass of the particles
Real(kind=dp), Parameter :: dt = 0.00001_dp !Time steps
Real(Kind=dp), Parameter , Dimension(1:3):: box = (/10.0_dp, 10.0_dp, 10.0_dp/) !dimensions of the simulation box.

Real(Kind=dp) , Parameter :: eps=1.0_dp, sigma = 0.01_dp

!Variables
Real(kind=dp), Dimension(nparts) :: x_pos, y_pos, z_pos
Real(Kind=dp), Dimension(nparts) :: x_vel, y_vel, z_vel
Real(Kind=dp), Dimension(nparts) :: x_force, y_force, z_force
Real(Kind=dp), Dimension(nparts) :: x_accel, y_accel, z_accel
Real(Kind=dp), Dimension(nparts) :: cutoff

Real(Kind=dp) :: e_potential, e_kinetic, E0
Real(Kind=dp) :: t1, t

End Module md_data

Program md

use md_data
use omp_lib
Implicit None
Integer :: i

!Initialise the particles
Call Initialise()

Call compute_step()
t=0.0_dp
do i=1, nsteps
  E0 = 0.0_dp
  t1 = omp_get_wtime()
  call compute_step()
  call update()
  t = t + omp_get_wtime() - t1
  print *, "energy", E0
end do

  print *, "Runtime for loops", t, "s."


End Program md

Subroutine initialise()

use md_data
use omp_lib
use twister

Implicit None

Integer, Allocatable, dimension(:) :: seeds
Integer :: i,j,z
Real(Kind=dp) :: x

!Set the random seed so we always get the same particles.
call twisterInit(0)

do i=1,nparts
x = (real(EXTRACTU(), dp)/real(MAX_RAND, dp));
        x_pos(i) = x * box(1);
x = (real(EXTRACTU(), dp)/real(MAX_RAND, dp));
        y_pos(i) = x * box(2);
x = (real(EXTRACTU(), dp)/real(MAX_RAND, dp));
        z_pos(i) = x * box(3);
    cutoff(i) = real(i, dp) * (box(1) / real(nparts, dp))
    !Create a random cutoff for each particle - load imbalance! Pairs interact if either particle's cutoff is true.
end do
x_vel = 0.0_dp
y_vel = 0.0_dp
z_vel = 0.0_dp
x_force = 0.0_dp
y_force = 0.0_dp
z_force = 0.0_dp
x_accel = 0.0_dp
y_accel = 0.0_dp
z_accel = 0.0_dp

End Subroutine initialise

Subroutine compute_step()
use  md_data
Implicit None

Real(Kind=dp) :: v, dv, x
Integer :: i, j, k
Real(Kind=dp) :: rxx, ryy, rzz
Real(Kind=dp) :: rrr, rsq, ir, irsq
Real(Kind=dp) :: cutoffs
Real(Kind=dp) :: force, energy

!One-sided force interaction
!$omp parallel do default(none) shared(x_pos, y_pos, z_pos, x_force, y_force, z_force, cutoff) &
!$omp private(rxx, ryy, rzz, cutoffs, rsq, rrr, force, energy) reduction(+:E0) schedule(runtime)
do i=1, nparts
    do j=1, nparts
        if(i==j) CYCLE
        rxx = x_pos(i) - x_pos(j)
        ryy = y_pos(i) - y_pos(j)
        rzz = z_pos(i) - z_pos(j)
        cutoffs = max(cutoff(i), cutoff(j))
        rsq = rxx*rxx + ryy*ryy + rzz*rzz
        if(rsq < cutoffs*cutoffs) then
        !Compute interaction
         rrr = sqrt(rsq)
         call compute_v(rrr, force, energy)
         !Store energy due to particle i
         E0 = E0 + 0.5_dp*energy
         x_force(i) = x_force(i) + force*rxx
         y_force(i) = y_force(i) + force*ryy
         z_force(i) = z_force(i) + force*rzz
        
        end if
    end do
    
end do
!$omp end parallel do
End subroutine compute_step

Subroutine compute_v(dist, force, energy)
Use md_data
Implicit None

Real(Kind = dp), Intent(In) :: dist
Real(Kind = dp), Intent(Out) :: force, energy
Real(Kind=dp) :: sig_r, sig_r2, sig_r6, sig_r12

sig_r = sigma/dist
sig_r2 = sig_r * sig_r
sig_r6 = sig_r2 * sig_r2 * sig_r2
sig_r12 = sig_r6 * sig_r6

force = 4.0_dp*eps*(sig_r12 - sig_r6)
energy = 4.0_dp*eps*sig_r6*(sig_r6-1.0_dp)

End Subroutine compute_v

Subroutine update()
Use md_data
Implicit None

Integer :: i,j
Real(Kind=dp) :: rmass

rmass = 1.0_dp / mass
!$omp parallel do default(none) shared(x_pos, y_pos, z_pos, x_vel, y_vel, z_vel) &
!$omp shared(x_accel, y_accel, z_accel, x_force, y_force, z_force, rmass) private(i)
do i = 1, nparts
  x_pos(i) = x_pos(i) + x_vel(i)*dt + 0.5*dt*dt*x_accel(i)
  y_pos(i) = y_pos(i) + y_vel(i)*dt + 0.5*dt*dt*y_accel(i)
  z_pos(i) = z_pos(i) + z_vel(i)*dt + 0.5*dt*dt*z_accel(i)

  x_vel(i) = x_vel(i) + 0.5*dt*(x_force(i)*rmass + x_accel(i))
  y_vel(i) = y_vel(i) + 0.5*dt*(y_force(i)*rmass + y_accel(i))
  z_vel(i) = z_vel(i) + 0.5*dt*(z_force(i)*rmass + z_accel(i))
  
  x_accel(i) = x_force(i)*rmass
  y_accel(i) = y_force(i)*rmass
  z_accel(i) = z_force(i)*rmass
  
  
  
end do
End Subroutine
