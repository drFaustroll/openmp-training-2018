import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
import numpy as np
from natsort import natsorted, index_natsorted, order_by_index
from os import listdir
from os.path import isfile, join
from matplotlib.animation import FuncAnimation

onlyfiles = [f for f in listdir('.') if isfile(join('.', f))]
onlyoutputs = [f for f in onlyfiles if ".out" in f]

output_files = natsorted(onlyoutputs)

frames = [ np.genfromtxt(i,delimiter=' ') for i in output_files]

def animate(frame):
  global frames, image
  image.set_array(frames[frame])
  return image


maxdim = max(len(frames[0]),len(frames[0][0]))
sizex =  len(frames[0])/maxdim
sizey = len(frames[0][0])/maxdim

fig, ax = plt.subplots(1,figsize=(8*sizey,8*sizex))
fig.subplots_adjust(0,0,1,1)
ax.axis("off")
cmap = colors.ListedColormap(['white', 'black'])
bounds = [0.0,0.5,1.0]
norm = colors.BoundaryNorm(bounds,cmap.N)
image = ax.imshow(frames[0], cmap=cmap, norm=norm)



animation = FuncAnimation(fig,animate,np.arange(len(output_files)), fargs=[], interval = 250)

animation.save("animation.gif",dpi=512)
