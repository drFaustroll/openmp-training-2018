#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>

void output_board(char **board, int width, int height){
  for(int x = 0; x < height; x++){
    for(int y = 0; y < width; y++){
      if(board[x][y]){
          printf("1 ");
      }else{
          printf("0 ");
      }
    }
    printf("\n");
  }
  printf("\n\n");
  fflush(stdout);
}

void evolve_step(char **board, char **newboard, int width, int height){
    for(int x = 0; x < height; x++){
      for(int y = 0; y < width; y++){
        int n = 0;
        for(int x1 = x-1; x1 <= x+1; x1++){
          for(int y1 = y-1; y1 <= y+1; y1++){
            int tempx = x1, tempy = y1;
            /* Here we check for the edge cases*/
            if(tempy < 0) tempy += width;
            if(tempy >= width) tempy -= width;
            if(tempx < 0) tempx += height;
            if(tempx >= height) tempx -= height;
            if(board[tempx][tempy]) n++;
          }
        }
        //Don't count the cell itself
        if(board[x][y]) n--;
        newboard[x][y] = (n==3 | (n==2 && board[x][y]));
      }
    } 
}

void init_random(char **board, int width, int height){
  for(int x = 0; x < height; x++){
    for(int y = 0; y < width; y++){
      int temp = rand();
      if(temp < (RAND_MAX/10) ){
        board[x][y] = 1;
      }else{
        board[x][y] = 0;
      }
    }
  }
}


void init_block(char **board, int width, int height){
  if(width >= 3 && height >= 3){
    for(int x = 0; x < height; x++){
      for(int y = 0; y < width; y++){
        if((x == 0 || x == 1) && (y == 0 || y == 1)){
          board[x][y] = 1;
        }else{
          board[x][y] = 0;
        }
      }
    }
  }else{
    return;
  }
}

void init_blinker(char **board, int width, int height){
  if(width >=6 && height >=6){
    for(int x = 0; x < height; x++){
      for(int y = 0; y < width; y++){
        if(x==1 && (y > 0 && y < 4)){
          board[x][y] =1;
        }else{
          board[x][y] = 0;
        }
      }
    }
  }else{
    return;
  }
}

void init_glider(char **board, int width, int height){
  if(width >= 10 && height >= 10){
    for(int x = 0; x < height; x++){
      for(int y = 0; y < width; y++){
        if(y == 2 && x < 3){
          board[x][y] = 1;
        }else if(y==1 && x==2){
          board[x][y] = 1;
        }else if(y==0 && x==1){
          board[x][y] = 1;
        }else{
          board[x][y] = 0;
        }
      }
    }
  }
}

int main(int argc, char *argv[]){

  char **board;
  char **board2;
  char **temp;
  int width = 2048;
  int height = 2048;
  
  board = malloc(sizeof(char*) * height);
  board2 = malloc(sizeof(char*) * height);
  for(int i = 0; i < width; i++){
    board[i] = malloc(sizeof(char)*width);
    board2[i] = malloc(sizeof(char)*width);
  }


  init_random(board, width, height);
//  output_board(board, width, height);
  double start = omp_get_wtime();
  for(int i = 0; i < 250; i++){
  evolve_step(board, board2, width, height);
//  output_board(board2, width, height);
  temp = board;
  board = board2;
  board2 = temp;
  }
  double finish = omp_get_wtime();
  printf("Game of life took %f seconds.\n", finish-start);

//  output_board(board, width, height);

  for(int i = 0; i < width; i++){
    free(board[i]);
    free(board2[i]);
  }
  free(board);
  free(board2);
  return 0;
}
