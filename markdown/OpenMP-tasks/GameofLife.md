Game of Life Example
====================

## General

The `examples/OpenMP-tasks/GoL` directory contains all the code and scripts required for this exercise. This exercise aims to highlight the potential benefits and disadvantages that can come with OpenMP tasks. The examples do not require explicit coding, though you may find examining the source code so you can see how it works helpful.


## Part 1
Load the Intel compiler using `module load intel/latest`. In the `examples/OpenMP-tasks/GoL` folder, type `make` to build the first applications. You should end up with 3 binaries in the `bin` folder:
 
  1. `omp`: This is the Game of Life code parallelised using `#pragma omp for`. (GoLomp.c)
  2. `task`: This is the Game of Life code where each step is parallelised using tasks without dependencies. (GoLtask.c)
  3. `depend`: This implementation uses tasks and dependencies to parallelise timsteps as well as inside a single timestep. (GoLtask2.c)

Submit the scaling run using `bsub < efficiency.lsf`, which will take around 5-10 minutes to run. While its running look at the code and work out how each version is parallelised.

Once the job is complete, load python3 with anaconda (`module load python3/anaconda`) and run `python3 scaling.py`, which will create you a `scaling.png` containing a runtime plot, a scaling plot, and a parallel efficiency plot. The dependencies add a large amount of overhead at the start of the computation - resulting in poor runtime and scaling.

You can explore how different parameters or compilers affect the code by loading the gcc module (`module unload intel/latest`, `module load gcc8/8.1.0`) and rebuilding (`make clean && make`), or by editing the Makefile and changing the `DEF_WIDTH`,`DEF_HEIGHT`, and `DEF_TIMESTEPS` variables.


## Part 2
The second part highlights how dependencies can be advantageous, using a timeline visualisation of the computation.

Reload the Intel compiler, and build the full suite of binaries with `make clean && make all`. This creates 3 additional binaries, which are smaller testcases that create some additional output. To gather the data, run `bsub < timings.lsf`. This should only take a minute or so to run. Once it has finished, run `python3 depends.py`, which will create 3 images. In these images, the x axis represents a column in the game of life "_world_", and the y axis represents time. The dotted horizontal lines show where barriers occur in the computation. The different coloured rectangles show areas of computation, with the different colours distinguishing between each timestep. Lines between rectangles denote the dependencies. To view these plots you can download them from the machine to the workstation with `scp`, or view them on the login node using `display`.

This script ignores any overheads that occur before the computation begins. You can easily compare the performance of each of the variants, and how different the order of computation is.

Again, explore how different parameters of compilers affect the code. To change the parameters, alter the `TIMING_WIDTH`, `TIMING_HEIGHT`, and `TIMING_TIMESTEPS` variables in the Makefile.

## Animations
The included Game of Life also can make animations to show the evolution of a system. Load the required sources with `source /lustre/scafellpike/local/HCT01/train/shared/OpenMPIntelTraining/temp.sh`. Running `bsub < animation.lsf` will run a job and create outputs in the `outputs/` directory. If you go into the `outputs` directory and run `python3 animation.py` it should create you `animation.gif` You can change the size of the animated system by editing the `OUTPUT_WIDTH`, `OUTPUT_HEIGHT` and `OUTPUT_TIMESTEPS` values in the makefile and resubmitting the job. To view the animations, you'll need to use `scp` to copy them to the workstation.
