Hartree Centre OpenMP Training Materials and Exercises
======================================================

Introduction
------------
Welcome to the Hartree Centre *Accelerating codes on Intel Processors* Training course. In this course you will learn to evaluate software performance and optimise applications to take full advantage of the latest Intel Xeon and Xeon Phi processors (like the one found on the Scafell Pike machine). For this purpose we will teach multiple aspects of OpenMP 4.5 such as Worksharing, SIMD and Task constructs. We will also showcase the Intel Performance analysis tools: Intel Vector Advisor and Intel Vtune that will help you better understand the performance characteristics of an application. Finally you will learn to use numerical libraries already optimized for the processors.

During the third day we will conduct a *Bring your own code* session.

Lecturers
---------
- Sergi Siso (Hartree Centre)
- Aidan Chalk (Hartree Centre)
- Alin Elena (Scientific Computing Department)
- James Clark (Hartree Centre)
- Heinrich Bockhorst (Intel)
- Klaus-Dieter Oertel (Intel)

Lecture Materials
-----------------

The Welcome presentation can be found :download:`Welcome slides <./Welcome.pdf>`.

Lecture 1 is on :download:`OpenMP Worksharing constructs <./OpenMP-worksharing-training.pdf>`. This introduces you to basic OpenMP, such as OpenMP :code:`for`/:code:`do` constructs, as well as the data sharing clauses commonly used in OpenMP programs, such as :code:`shared` and :code:`private`.

Lecture 2 covers :download:`Vectorisation and the OpenMP SIMD pragmas <./OpenMP-simd-training.pdf>`. The lecture shows why vectorisation is important to utilise modern hardware efficiently, and the options available for you to exploit this feature through OpenMP and language features.

Lecture 3 covers :download:`Task-Based Parallelism with OpenMP <./OpenMP-task-training.pdf>`. This lecture explains an alternative parallel paradigm, Task-Based Parallelism, and how to use this paradigm with OpenMP.


Each of the lectures have corresponding coding examples, and the initial codebases are available on Gitlab_.

Use `git clone  https://gitlab.com/hartreetraining/openmp-training.git` to download the exercises after :doc:`logging into Scafell Pike.</login>`

A pdf of the documentation is available :download:`also <./OpenMPTraining.pdf>`.

.. _Gitlab: https://gitlab.com/sergisiso/OpenMP-Training/


Exercises
---------



.. toctree::
   :maxdepth: 1

   Home<index.rst>
   login.rst
   OpenMP-worksharing.rst
   OpenMP-simd.rst
   GameofLife.rst
   MolecularDynamics.rst
   nllab.rst
