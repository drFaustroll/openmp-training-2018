Machine Access
==============

## Using Scafell Pike
Connect to the machine with `ssh -YC hcxlogin1.hartree.stfc.ac.uk`.

Access the primary filesystem with `cd $HCBASE`. You can load all the modules you will need for the OpenMP sections of this course with `module load intel-training/anaconda`. For the Intel tools sections, use `module load intel-training/intel_tools`

The machine uses the lsf job submission system, so to submit a job you use `bsub < job.lsf`, to check status of jobs you use `bjobs`, and to kill a job use `bkill <jobid>`.

All job scripts must contain a wall time (specified with `-W hr:min`). To submit jobs to the Skylake nodes use `-q scafellpikeSKL -U intel_skl@scafell-pike`, and to submit jobs to the KNL nodes use `-q scafellpikeKNL -U intel_knl@scafell-pike`. To use these in a job script, each parameter is on a different line at the head of the script, and would be `#BSUB -q scafellpikeSKL` and `#BSUB -U intel_skl@scafell-pike`.

You can find more information on how to use Scafell Pike and lsf at [Hartree Wiki](http://community.hartree.stfc.ac.uk/wiki/site/admin/sfp_jobs.html).
